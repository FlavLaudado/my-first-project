/*
   LUCIERNAGUITAS
*/

#include <Adafruit_NeoPixel.h>
#ifdef __AVR__
#include <avr/power.h>
#endif

const int PIN = 6;
const int NUMPIXELS = 3;

Adafruit_NeoPixel strip = Adafruit_NeoPixel(NUMPIXELS, PIN, NEO_GRB + NEO_KHZ800);

//COLORES
int r[] = {70, 3, 25, 70, 3, 25};
int g[] = {25, 70, 3, 25, 70, 3};
int b[] = {3, 25, 70, 3, 25, 70};

//hue = 0-65535
int h[] = {298, 195, 79, 320, 19, 230};
int sat = 255;
int val = 178;
int v[6];

int pinLED = 13;

int monedas[6];

int pasosPrender = 100;
int timeOn = 500;

void setup() {
  //declarar LEDs
  Serial.begin(9600);

  // This is for Trinket 5V 16MHz, you can remove these three lines if you are not using a Trinket
#if defined (__AVR_ATtiny85__)
  if (F_CPU == 16000000) clock_prescale_set(clock_div_1);
#endif
  // End of trinket special code

  strip.begin();
  strip.show();
  //strip.setBrightness(50);//de 0-255
}

void loop() {
  //tirar monedas
  for (int i = 0; i < NUMPIXELS; i++) {
    monedas[i] = tirarMoneda();

  }
  escribirEnter("fin ciclo for monedas");

  //prender con color definido
  for (int i = 0; i < NUMPIXELS; i++) { //va de 0-5

    if (monedas[i] == 1) {
      //función que prenda el LED con el value que le toca
      for (int j = 0; j <= pasosPrender; j++) { //va de 0-100
        v[i] = map( j, 0, pasosPrender, 0, val);
        //analogWrite( pinLED, v[i]);
        strip.setPixelColor(i, strip.ColorHSV(h[i] * 182, sat, v[i]));
        strip.show();
        escribir("moneda:");
        escribir(i);
        escribir("h:");
        escribir(h[i]);
        escribir("s:");
        escribir(sat);
        escribir("v:");
        escribir(v[i]);
        escribirEnter(" ");
        delay(100);
      }
    }

  }
  apagarLEDs();
  delay(timeOn);

}

int tirarMoneda() {
  int rta = int(random(2));
  Serial.println(rta);
  return rta;
}

void apagarLEDs() {
  for (int i = 0; i < NUMPIXELS; i++) {
    strip.setPixelColor(i, strip.Color(0, 0, 0));
  }
  strip.show();
}

void escribir(String texto) {
  Serial.print(" ");
  Serial.print(texto);
  Serial.print(" ");
}
void escribir(int valor) {
  Serial.print(" ");
  Serial.print(valor);
  Serial.print(" ");
}
void escribirEnter(String texto) {
  Serial.println(texto);
  Serial.println(" ");
}
